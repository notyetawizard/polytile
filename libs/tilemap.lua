----------------------------------------------------------------------
-- Tilemap Class & Constructor ---------------------------------------
----------------------------------------------------------------------
local Tilemap = {}

-- Tilemap constructor.
-- Mandadatory argument: name must be a name for the Tilemap, as a string.
-- Optional argument: tiles can be from a loaded tilemap.
function Tilemap.new(name, tiles)
	------------------------------------------------------------------
	-- Assertions, to make sure you're using this correctly ----------
	------------------------------------------------------------------
	assert(type(name) == "string", "Argument must be a map name, as a string")
	if tilemap then
		assert(type(tiles) == "table", "tiles must be table of Polytile tiles")
	end

	------------------------------------------------------------------
	-- Some safe, out of scope, per object variables -----------------
	------------------------------------------------------------------
	local name = name -- Name of the Tilemap
	local tiles = tiles or { -- Tiles, in x, y, z
		[0] = {
			[0] = {
				[0] = 1
			}
		}
	}
	local minx = 0 -- Lowest x value tile.
	local maxx = 0 -- Highest x value tile.
	local miny = 0 -- Lowest y value tile.
	local maxy = 0 -- Highest y value tile.
	local minz = 0 -- Lowest z value tile.
	local maxz = 0 -- Highest z value tile.

	------------------------------------------------------------------
	-- Tilemap Methods -----------------------------------------------
	------------------------------------------------------------------
	local object = {}
	setmetatable(object, Tilemap)

	-- Returns the name of the Tilemap
	function object.getName()
		return name
	end

	-- Sets the name of the Tilemap
	function object.setName(new_name)
		assert(type(new_name) == "string", "Argument must be a map name, as a string")
		name = name
	end

	-- Returns the current size of the Tilemap
	-- Return order:
	-- lowest x, highest x
	-- lowest y, highest y
	-- lowest z, highest z
	-- total number of tiles (some maybe nil)
	function object.getSize()
		local dx = maxx - minx + 1
		local dy = maxy - miny + 1
		local dz = maxz - minz + 1
		local total = dx * dy * dz
		return minx, maxx, miny, maxy, minz, maxz, total
	end

	-- Resets the current size of the Tilemap, by looking through all tiles.
	function object.resetSize()
		local new_minx = 0
		local new_maxx = 0
		local new_miny = 0
		local new_maxy = 0
		local new_minz = 0
		local new_maxz = 0
		for xk, xv in pairs(tiles) do
			if xk < new_minx then
				new_minx = xk
			elseif xk > new_maxx then
				new_maxx = xk
			end
			for yk, yv in pairs(xv) do
				if yk < new_miny then
					new_miny = yk
				elseif yk > new_maxy then
					new_maxy = yk
				end
				for zk, zv in pairs(yv) do
					if zk < new_minz then
						new_minz = zk
					elseif zk > new_maxz then
						new_maxz = zk
					end
				end
			end
		end
		minx = new_minx
		maxx = new_maxx
		miny = new_miny
		maxy = new_maxy
		minz = new_minz
		maxz = new_maxz
	end

	-- Returns the tilekey of the tile at x, y, z if it exists
	-- Mandatory arguments: x, y, z must be numbers representing tile position.
	function object.getTile(x, y, z)
		assert(type(x) == "number", "x, y, and z values are required!")
		assert(type(y) == "number", "x, y, and z values are required!")
		assert(type(z) == "number", "x, y, and z values are required!")
		if not tiles[x] or not tiles[x][y] or not tiles[x][y][z] then
			return nil
		else
			return tiles[x][y][z]
		end
	end

	-- Sets the tile at x, y, z to the tilekey
	-- Mandatory arguments: x, y, z must be numbers representing tile position.
	function object.setTile(x, y, z, tilekey)
		assert(type(x) == "number", "x, y, and z values are required!")
		assert(type(y) == "number", "x, y, and z values are required!")
		assert(type(z) == "number", "x, y, and z values are required!")
		if not tiles[x] then tiles[x] = {} end
		if not tiles[x][y] then tiles[x][y] = {} end
		if not tiles[x][y][z] then
			if x <= minx then
				minx = x
			elseif x >= maxx then
				maxx = x
			end
			if y <= miny then
				miny = y
			elseif y >= maxy then
				maxy = y
			end
			if z <= minz then
				minz = z
			elseif z >= maxz then
				maxz = z
			end
		end
		tiles[x][y][z] = tilekey
	end

	-- An iterator to be used in for loops
	-- Returns a control, x, y, and z values
	function object.itiles()
		local dx = maxx - minx + 1
		local dy = maxy - miny + 1
		local dz = maxz - minz + 1
		local control_end = dx * dy * dz
		local control = -1

		local function iterator(tiles, control)
			control = control + 1
			if control == control_end then return nil end
			local z = (control % dz)
			local y = (control - z)/dz % dy
			local x = (control - z - y)/dz/dy % dx
			return control, minx + x, miny + y, minz + z
		end

		return iterator, tiles, control
	end

	return object
end

-- NEEDS REWRITE FOR USE!
-- Construct a new Tilemap from a file.
-- Mandatory argument: filename must be for an existing file.
-- This loads external code, and can fuck things up if you let it.
function Tilemap.load(filename)
	assert(type(filename) == "string", "Argument must be a filename, as a string")
	assert(love.filesystem.exists(filename), "The given file does not exist!")
	local loaded = love.filesystem.load(filename)()
	local object = Tilemap.new(loaded.name, loaded.tiles)
	object.resetSize()
	return object
end

-- NEEDS REWRITE FOR USE!
-- Tilemap writer
function Tilemap.save(tilemap, filename)
	assert(getmetatable(tilemap) == Tilemap, "This is not a polytile Tilemap object!")
	if not filename then filename = "maps/"..map.name..".lua" end
	-- I dunno, write the map.
end

return Tilemap
