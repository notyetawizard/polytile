local polytile = require "init"

function love.load()
	testtileset = polytile.Tileset.load("test-tileset.lua")
	testmap = polytile.Tilemap.new("test-map")

	for x = -4, 4 do
		for y = -2, 2 do
			local c = math.random(1, 3)
			testmap.setTile(x, y, 0, c)
		end
	end

	editor = polytile.Editor.new(testtileset, testmap)
	editor.start()
end

function love.mousepressed(x, y, button, istouch)

end

function love.keypressed(key, scan, isrepeat)

end

function love.update()

end

function love.draw()

end
