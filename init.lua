local polytile = {}

polytile.Tileset = require "libs/tileset"
polytile.Tilemap = require "libs/tilemap"
polytile.Camera = require "libs/camera"
polytile.Editor = require "libs/editor"

return polytile