test = {
	name = "test",
	tilesize = 16,
	tiles = {
		[1] = {
		    name = "red",
			color = {255, 0, 0}
		},
		[2] = {
		    name = "green",
			color = {0, 255, 0}
		},
		[3] = {
		    name = "blue",
			color = {0 , 0, 255}
		}
	}
}

return test
