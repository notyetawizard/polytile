----------------------------------------------------------------------
-- Editor Class & Constructor ----------------------------------------
----------------------------------------------------------------------
local Tileset = require "libs/tileset"
local Tilemap = require "libs/tilemap"
local Camera = require "libs/camera"

local Editor = {} -- Class, exported

-- Editor constructor.
-- Mandadatory arguments: tileset and tilemap must be valid Polytile objects.
function Editor.new(tileset, tilemap)
    ------------------------------------------------------------------
	-- Assertions, to make sure you're using this correctly ----------
	------------------------------------------------------------------
    assert(getmetatable(tileset) == Tileset, "This is not a polytile Tileset object!")
	assert(getmetatable(tilemap) == Tilemap, "This is not a polytile Tilemap object!")
    ------------------------------------------------------------------
	-- Some safe, out of scope, per object variables -----------------
	------------------------------------------------------------------
    local camera = Camera.new(tileset, tilemap)
    local panning = false
    local tilelist = tileset.getTilekeys()
    local ctile = next(tilelist)
    print(ctile)

    ------------------------------------------------------------------
	-- Editor Methods ------------------------------------------------
	------------------------------------------------------------------
    local object = {}
    setmetatable(object, Editor)

    function object.mousepressed(x, y, button, istouch )
        if button == 1 then
            local tx, ty = camera.getTile(x, y)
            tilemap.setTile(tx, ty, 0, ctile)
        elseif button == 2 then
            panning = true
        end
    end

    function object.mousereleased(x, y, button, istouch)
        if button == 1 then
        elseif button == 2 then
            panning = false
        end
    end

    function object.mousemoved(x, y, dx, dy, istouch)
        if panning == true then
            camera.move(dx, dy, {type = "pixel", instant = true})
        end
    end

    function object.wheelmoved(dx, dy)
        -- test these values at somepoint. Could do better than >?
        if dy > 0 then
            camera.setScale(2)
        elseif dy < 0 then
            camera.setScale(1)
        end
    end

    function object.keypressed(key, scan, isrepeat)
    	if key == "rctrl" then
    		debug.debug()
    	elseif key == "up" then
    		camera.move(0, -1)
    	elseif key == "down" then
    		camera.move(0, 1)
    	elseif key == "left" then
    		camera.move(-1, 0)
    	elseif key == "right" then
    		camera.move(1, 0)
    	elseif key == "a" then
    		camera.center(0, 0)
        elseif key == "o" then
            ctile = next(tilelist, ctile)
    	end
    end

    function object.update(dt)
    	camera.update(dt)
    end

    function object.draw()
        camera.draw()
    end

    function object.start()
        panning = false
        camera.center(0, 0, {instant = true})

        love.mousepressed = object.mousepressed
        love.mousereleased = object.mousereleased
        love.mousemoved = object.mousemoved
        love.wheelmoved = object.wheelmoved
        love.keypressed = object.keypressed
        love.update = object.update
        love.draw = object.draw
    end

    function object.stop()
        panning = false

        love.mousepressed = nil
        love.mousereleased = nil
        love.mousemoved = nil
        love.wheelmoved = nil
        love.keypressed = nil
        love.update = nil
        love.draw = nil
    end

    return object
end


function Editor.stop()
    for k, v in pairs(hidden) do
       hidden[k] = nil
    end

    for k, v in pairs(methods) do
        love[k] = nil
    end
end

return Editor
