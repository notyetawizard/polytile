----------------------------------------------------------------------
-- Camera Class & Constructor ----------------------------------------
----------------------------------------------------------------------
local Tileset = require "libs/tileset"
local Tilemap = require "libs/tilemap"

local Camera = {}

-- Unexported metatable with default values for camera methods that take options.
local meta_options = {
	__index = {
		type = "tile",
		instant = false
	}
}

local function set_meta_options(options)
	if type(options) ~= "table" then
		return meta_options.__index
	else
		setmetatable(options, meta_options)
		return options
	end
end

-- Camera constructor.
-- Mandadatory arguments: tileset and tilemap must be valid Polytile objects.
function Camera.new(tileset, tilemap)
	------------------------------------------------------------------
	-- Assertions, to make sure you're using this correctly ----------
	------------------------------------------------------------------
	assert(getmetatable(tileset) == Tileset, "This is not a polytile Tileset object!")
	assert(getmetatable(tilemap) == Tilemap, "This is not a polytile Tilemap object!")

	------------------------------------------------------------------
	-- Some safe, out of scope, per object variables -----------------
	------------------------------------------------------------------
	local cx = 0 -- Current x position
	local cy = 0 -- Current y position
	local nx = 0 -- Target x position
	local ny = 0 -- Target y position
	local scale = 1 -- Drawing scale
	local tilesize = tileset.getTilesize() -- Size of a tile, in pixels.
	local speed = 4*tilesize -- Panning speed


	------------------------------------------------------------------
	-- Camera Methods ------------------------------------------------
	------------------------------------------------------------------
    local object = {}
    setmetatable(object, Camera)

	-- Change the tileset the camera is using.
	-- Takes a Polytile Tileset object as an argument
    function object.setTileset(new_tileset)
		assert(getmetatable(new_tileset) == Tileset, "This is not a polytile Tileset object!")
    	tileset = new_tileset
		tilesize = tileset.getTilesize()
    end

	-- Change the tilemap the camera is using.
	-- Takes a Polytile Tilemap object as an argument
    function object.setTilemap(new_tilemap)
    	assert(getmetatable(new_tilemap) == Tilemap, "This is not a polytile Tilemap object!")
    	tilemap = new_tilemap
    end

	-- Sets the camera's panning speed
	-- Mandatory argument: s must be a number representing speed.
	-- Also takes an options table as an argument.
	-- If options.type == "tile" (default) then speed is in tiles per second.
    -- If options.type == "pixel" then speed is in pixels per second.
    function object.setSpeed(s, options)
        assert(type(s) == "number", "A speed value is required!")
		local options = set_meta_options(options)

        if options.type == "tile" then
            speed = s*tileset.tilesize
        elseif options.type == "pixel" then
            speed = s
        end
    end

    -- UNFINISHED
	function object.setScale(s)
		assert(type(s) == "number", "A scale value is required!")
		scale = s
	end

	-- Returns the x, y values of the tile at screen position x, y
	-- Mangadory arguments: x, y must be numbers representing screen position.
	function object.getTile(x, y)
	    assert(type(x) == "number", "Both and x and y values are required!")
    	assert(type(y) == "number", "Both and x and y values are required!")
		local tx = math.floor((x - cx)/tilesize/scale)
		local ty = math.floor((y - cy)/tilesize/scale)
		return tx, ty
	end

	-- Returns the x, y values for upper left corner of a tile on the screen
	function object.getPixel(x, y)
		assert(type(x) == "number", "Both and x and y values are required!")
    	assert(type(y) == "number", "Both and x and y values are required!")
		local sx = math.floor((x*tilesize*scale + cx))
		local sy = math.floor((y*tilesize*scale + cy))
		return sx, sy
	end

	-- Move the camera by x, y to look at something else.
	-- Mandatory arguments: x, y must be numbers representing movement distance.
	-- Also takes an options table as an argument.
	-- If options.type == "tiles" (default) then movement will be by number of tiles.
	-- If options.type == "pixel" then movement will be by number of pixels.
	-- If options.instant == true then movement will ignore the camera's panning speed.
    function object.move(x, y, options)
    	assert(type(x) == "number", "Both and x and y values are required!")
    	assert(type(y) == "number", "Both and x and y values are required!")
    	local options = set_meta_options(options)

        if options.type == "tile" then
            nx = cx + x*tilesize*scale
    		ny = cy + y*tilesize*scale
    	elseif options.type == "pixel" then
    		nx = cx + x
    		ny = cy + y
    	end

    	if options.instant == true then
    		cx, cy = nx, ny
    	end
    end

    -- Center the camera on x, y to look at something else.
	-- Mandatory arguments: x, y must be numbers representing a point.
	-- Also takes an options table as an argument.
	-- If options.type == "tiles" (default) then the point will be a tile.
	-- If options.type == "pixel" then the point will be a pixel.
	-- If options.instant == true then movement will ignore the camera's panning speed.
    function object.center(x, y, options)
    	assert(type(x) == "number", "Both and x and y value are required!")
    	assert(type(y) == "number", "Both and x and y value are required!")
    	local options = set_meta_options(options)

    	if options.type == "tile" then
    		local sx, sy = object.getPixel(x, y)
    		nx = love.graphics.getWidth()/2 + cx - sx - tilesize*scale/2
    		ny = love.graphics.getHeight()/2 + cy - sy - tilesize*scale/2
    	elseif options.type == "pixel" then
    		nx = love.graphics.getWidth()/2 + x
    		ny = love.graphics.getHeight()/2 + y
    	end

    	if options.instant == true then
    		cx, cy = nx, ny
    	end
    end

	-- To be inserted into love.update(dt).
	-- Required for any non-instant movement calls!
	-- Takes dt (time since last called) as an argument
    function object.update(dt)
    	local dx = nx - cx
    	local dy = ny - cy
    	if dx == 0 and dy == 0 then return end
    	if math.sqrt(dx^2 + dy^2) < dt*speed then
    		cx, cy = nx, ny
    	else
    		cx = cx + dt*speed*scale*(dx/(math.abs(dx)+math.abs(dy)))
    		cy = cy + dt*speed*scale*(dy/(math.abs(dx)+math.abs(dy)))
    	end
    end

	-- To be inserted into love.draw()
	-- Required to see what the camera sees!
	function object.draw()
    	love.graphics.translate(cx, cy)
    	love.graphics.scale(scale)
    	local p_color = {love.graphics.getColor()}
    	for _, x, y, z in tilemap.itiles() do
    		local tile = tileset.getTile(tilemap.getTile(x, y, z))
    		if tile then
    			if tile.image then
    				--draw it
    			elseif tile.color then
    				love.graphics.setColor(tile.color)
    				love.graphics.rectangle(
    					"fill",
    					x*tilesize,
    					y*tilesize,
    					tilesize,
    					tilesize
    					)
    				love.graphics.setColor(p_color)
    			end
    		end
    	end
	end

	return object
end

return Camera
