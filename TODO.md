# Next

- Hella simple visual map editor:
- ~~Right click and mouse to pan around~~
- ~~Scroll wheel to zoom~~
- Left click to place tile
- Keys to select tile (include delete)
- Minimal text info up top (mapname, tileset, mapsize, current tile, tile xy)
- ~~Debug key to do console things?~~
- Checkout https://github.com/Ranguna/LOVEDEBUG
- Looks nicer than swapping to a connected term over and over, maybe?

- Tilemap:save() isn't actually written, and will be needed!

# Always
- Clean code for a library!
- Set and check metatables for safety.
- That is, safe but not *too* safe. Can't be slow.

# Later

- Objects layer (multitile)
- Smoothed physics.

# Eventually

- Finish writing image/png portions of tileset
- I guess I need to install something to edit pixel art?
- Add z height offset, for tiles with some vertical walls/height.

# Maybe Never

- Multi perspective + rotations

# Finished

- ~~Get some fucking tiles drawing where they should. Each tile is just a coloured rectangle?~~
- ~~Mapping system.~~
- ~~Layers.~~
- ~~Use tilesets and tilekeys~~
- ~~Create a testing tileset~~
- ~~Create a testing map~~
- ~~Test them~~
- ~~Move tilesize into Tileset, not Tilemap.~~
- ~~Cameraing system for it~~