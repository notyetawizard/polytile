----------------------------------------------------------------------
-- Tileset Class & Constructor ---------------------------------------
----------------------------------------------------------------------
local Tileset = {} -- Class, exported

-- Tileset constructor.
-- Mandadatory argument: name must be a name for the Tileset, as a string.
-- Optional arguments: tilesize and tiles can be from a loaded tilemap.
function Tileset.new(name, tilesize, tiles)
	------------------------------------------------------------------
	-- Assertions, to make sure you're using this correctly ----------
	------------------------------------------------------------------
	assert(type(name) == "string", "Argument must be a tileset name, as a string")
	if tilesize then
		assert(type(tilesize) == "number", "tilesize must be a number representing tilesize in pixels.")
	end
	if tiles then
		assert(type(tiles) == "table", "tiles must be a table of Polytile tiles.")
		for k, v in pairs(tiles) do
			assert(type(v.name) == "string", "Tile \"" ..k.. "\" is missing a name.")
		end
	end

	------------------------------------------------------------------
	-- Some safe, out of scope, per object variables -----------------
	------------------------------------------------------------------
	local name = name -- Name of the Tilemap
	local tilesize = tilesize or 1
	local tiles = tiles or {}

	------------------------------------------------------------------
	-- Tileset Methods -----------------------------------------------
	------------------------------------------------------------------
	local object = {}
	setmetatable(object, Tileset)

	-- Returns the tilesize for the Tileset
	function object.getTilesize()
		return tilesize
	end

	-- Returns a list of tilekeys in the Tileset
	function object.getTilekeys()
		local tilekeys = {}
		for k, v in pairs(tiles) do
			table.insert(tilekeys, k)
		end
		return tilekeys
	end

	-- Returns a tile from the Tileset
	-- Mandatory argument: tilekey must be a tilekey, or will return nil.
	function object.getTile(tilekey)
		if tilekey ~= nil and tiles[tilekey] == nil then
			print("The tilekey "..tilekey.." is not in this Tileset!")
		end
		return tiles[tilekey]
	end

	return object
end

-- Construct a new tileset from a file
-- Mandatory argument: filename must be for an existing file.
-- This loads external code, and can fuck things up if you let it.
function Tileset.load(filename)
	assert(type(filename) == "string", "Argument must be a filename, as a string")
	assert(love.filesystem.exists(filename), "The given file does not exist!")
	local loaded = love.filesystem.load(filename)()
	local object = Tileset.new(loaded.name, loaded.tilesize, loaded.tiles)
	return object
end

return Tileset
